<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = "teacher";
    public $timestamps = false;
    protected $fillable = ['teacher_name', 'birth_place', 'birth_date'];

    public function class(){
    	return $this->hasMany('App\Classes');
    }
}
