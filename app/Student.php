<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "student";
    public $timestamps = false;
    protected $fillable = ['student_name', 'birth_place', 'birth_date', 'gender'];

    public function class(){
    	return $this->belongsTo('App\Class');
    }
}
