<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\Classes;
use DataTables;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Authentication Checking
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.teacher.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Teacher();

        return view('pages.teacher.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'teacher_name' => 'required|string|max:100|unique:teacher,teacher_name',
            'birth_place' => 'required|string|max:150',
            'birth_date' => 'required'
        ]);

        $model = Teacher::create($request->all());

        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Teacher::findOrFail($id);

        return view('pages.teacher.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Teacher::findOrFail($id);

        return view('pages.teacher.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'teacher_name' => 'required|string|max:100|unique:teacher,teacher_name',
            'birth_place' => 'required|string|max:150',
            'birth_date' => 'required' 
        ]);

        $model = Teacher::findOrFail($id);

        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Teacher::findOrFail($id);

        Classes::where('teacher_id', $id)->update(['teacher_id' => null]);

        $model->delete();
    }

    public function dataTable()
    {
        $model = Teacher::query();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action', [
                    'model' => $model,
                    'url_show' => route('teacher.show', $model->id),
                    'url_edit' => route('teacher.edit', $model->id),
                    'url_destroy' => route('teacher.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
