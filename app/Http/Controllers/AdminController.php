<?php

namespace App\Http\Controllers;

use App\Classes; 
use DataTables;
use Illuminate\Http\Request;
use PDF;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.home');
    }

    public function pdf()
    {
        $get_data = Classes::leftJoin('student', 'class.id', '=', 'student.class_id')
            ->leftJoin('teacher', 'class.teacher_id', '=', 'teacher.id')
            ->select('class.*', 'student.*', 'teacher.teacher_name')
            ->get();

        $data = ['get_data' => $get_data];
        
        $pdf = PDF::loadView('pages.class.print', $data);
        
        return $pdf->stream('class_detail.pdf');	
    }
}
