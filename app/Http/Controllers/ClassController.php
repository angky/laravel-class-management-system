<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\Classes;
use App\Student;
use DataTables;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Authentication Checking
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.class.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Classes();

        $teacher = Teacher::orderBy('id')->get();

        return view('pages.class.form', compact('model'))
            ->with('listTeacher', $teacher);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'class_name' => 'required|string|max:100|unique:class,class_name',
            'teacher_id' => 'required|integer'
        ]);

        $model = Classes::create($request->all());

        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = Classes::findOrFail($id);

        return view('pages.class.show', compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Classes::findOrFail($id);

        $teacher = Teacher::orderBy('id')->get();

        return view('pages.class.form', compact('model'))
            ->with('listTeacher', $teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_name' => 'required|string|max:100',
            'teacher_id' => 'required|integer'
        ]);

        $model = Classes::findOrFail($id);

        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) 
    {
        $model = Classes::findOrFail($id);

        Student::where('class_id', $id)->update(['class_id' => null]);

        $model->delete();
    }

    public function dataTable()
    {
        $model = Classes::leftJoin('teacher', 'class.teacher_id', '=', 'teacher.id')
            ->select('class.*', 'teacher.teacher_name')
            ->get();

        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action_class', [
                    'model' => $model,
                    'url_show' => route('class.show', $model->id),
                    'url_edit' => route('class.edit', $model->id),
                    'url_destroy' => route('class.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
