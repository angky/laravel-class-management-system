<?php

namespace App\Http\Controllers;

use App\Student;
use DataTables;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Authentication Checking
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.student.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Student();

        return view('pages.student.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_name' => 'required|string|max:100|unique:student,student_name',
            'birth_place' => 'required|string|max:150',
            'birth_date' => 'required',
            'gender' => 'required'
        ]);

        $model = Student::create($request->all());

        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Student::findOrFail($id);

        return view('pages.student.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Student::findOrFail($id);

        return view('pages.student.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'student_name' => 'required|string|max:100|unique:student,student_name',
            'birth_place' => 'required|string|max:150',
            'birth_date' => 'required',
            'gender' => 'required'
        ]);

        $model = Student::findOrFail($id);
        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Student::findOrFail($id);
        $model->delete();
    }

    public function dataTable()
    {
        $model = Student::query();

        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action_student', [
                    'model' => $model,
                    'url_show' => route('student.show', $model->id),
                    'url_edit' => route('student.edit', $model->id),
                    'url_destroy' => route('student.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
