<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Student;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ClassDetailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detailClass($id)
    {
    	$detail = Classes::leftJoin('teacher', 'class.teacher_id', '=', 'teacher.id')
            ->select('class.*', 'teacher.teacher_name')
            ->where('class.id', '=', $id)
            ->get();
        
        return view('pages.detail.detail')
            ->with('detail', $detail);	    
    }

    public function dataTableClassDetail($id)
    {
    	$model = Classes::leftJoin('student', 'class.id', '=', 'student.class_id')
            ->select('class.*', 'student.*')
            ->where('class_id', '=', $id)
            ->get();

        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts._action_detail', [
                    'model' => $model,
                    'url_destroy' => route('destroyClassDetail', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function studentRegistration($id)
    {
        $model = Classes::findOrFail($id);
        $student = Student::where('class_id', '=', null)->get();

        return view('pages.detail.form', compact('model'))
        ->with('listStudent', $student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function studentClassUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'class_id' => 'required',
            'student_id' => 'required'
        ]);

        if (Classes::find($id)) {
        	Student::where('id', '=', Input::get('student_id'))
        	    ->update(['class_id' => $id]);	
        }
    }

    public function studentClassDelete(Request $request, $id)
    {
        Student::findOrFail($id);
        Student::where('id','=',$id)->update(['class_id' => null]);	
    }
}
