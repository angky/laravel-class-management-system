<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = "class";
    public $timestamps = false;
    protected $fillable = ['teacher_id', 'class_name'];

    public function teacher(){
    	return $this->belongsTo('App\Teacher');
    }

    public function student(){
    	return $this->hasMany('App\Student');
    }
}
