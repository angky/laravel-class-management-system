<a href="{{ $url_show }}" class="btn-show" title="Detail: {{ $model->class_name }}"><i class="icon-eye-open text-primary" ></i></a> | 
<a href="{{ $url_edit }}" class="modal-show edit" title="Edit {{ $model->class_name }}"><i class="icon-pencil text-inverse"></i></a> | 
<a href="{{ $url_destroy }}" class="btn-delete-class" title="{{ $model->class_name }}"><i class="icon-trash text-danger"></i></a> |
<a href="{{ route('detailClass', [$model->id]) }}" class="" title="{{ $model->class_name }}"><i class="icon-list text-success"></i></a>