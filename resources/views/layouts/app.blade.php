<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Class Management System">
        <meta name="author" content="Angky">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Class Management System - Laravel Test</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Datatables -->
        <link href="{{ asset('assets/vendor/datatables/datatables.min.css') }}" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="{{ asset('assets/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ asset('assets/css/navbar-fixed-top.css') }}" rel="stylesheet">

        @stack('styles')
    </head>
    <body>
        @guest
        @else
            <!-- Fixed navbar -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                       <a class="navbar-brand" href="#">CMS</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li class=""><a href="{{ route('teacher.index') }}">Teacher</a></li>
                            <li class=""><a href="{{ route('student.index') }}">Student</a></li>
                            <li class=""><a href="{{ route('class.index') }}">Class</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active">
                                <a href="{{ route('logout') }}" 
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Log Out 
                                    <span class="sr-only"></span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
             </nav>
            
            <section id="main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="list-group">
                                <a href="index.html" class="list-group-item active main-color-bg">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Dashboard
                                </a>
                                <a href="{{ route('teacher.index') }}" class="list-group-item">
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Teachers 
                                </a>
                                <a href="{{ route('student.index') }}" class="list-group-item">
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Students 
                                </a>
                                <a href="{{ route('class.index') }}" class="list-group-item">
                                    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Classes 
                                </a>
                            </div>
                        </div>
            @endguest
                        <div class="col-md-9">
                            @yield('content')
                        </div>
                    </div>    
                </div> <!-- /container -->
            </section>

        @include('layouts._modal')


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

        <!-- Datatables -->
        <script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>

        <!-- Sweetalert2 -->
        <script src="{{ asset('assets/vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="{{ asset('assets/js/ie10-viewport-bug-workaround.js') }}"></script>

        
        <script src="{{ asset('js/modal.js') }}"></script>

        @stack('scripts')
    </body>
</html>
