{!! Form::model($model, [
    'route' => $model->exists ? ['student.update', $model->id] : 'student.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

    <div class="form-group">
        <label for="" class="control-label">Student Name</label>
        <input type="text" name="student_name" value="{{ $model->student_name }}" class="form-control" id="student_name">
    </div>
    <div class="form-group">
        <label for="" class="control-label">Place Of Birth</label>
        <input type="text" name="birth_place" value="{{ $model->birth_place }}" class="form-control" id="birth_place">
    </div>
    <div class="form-group">
        <label for="" class="control-label">Date Of Birth</label>
        <input type="date" name="birth_date" value="{{ $model->birth_date }}" class="form-control" id="birth_date">
    </div>
    <div class="form-group">
        <label for="" class="control-label">Gender</label>
        <select name="gender" class="form-control" id="gender">
            @if ($model->gender == 'female')
                <option value="female" selected="true"> Female </option>
                <option value="male"> Male </option>
            @else
                <option value="male" selected="true"> Male </option>
                <option value="female"> Female </option>
            @endif
        </select>
    </div>

{!! Form::close() !!}
