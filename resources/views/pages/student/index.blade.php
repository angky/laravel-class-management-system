@extends('layouts.app')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Student Data
                <a href="{{ route('student.create') }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Create Student"><i class="icon-plus"></i> Add Student</a>
            </h3>
        </div>
        <div class="panel-body">
            <table id="datatable" class="table table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Place Of Birth</th>
                        <th>Date Of Birth</th>
                        <th>Gender</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                      
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Place Of Birth</th>
                        <th>Date Of Birth</th>
                        <th>Gender</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.student') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'student_name', name: 'student_name'},
                {data: 'birth_place', name: 'birth_place'},
                {data: 'birth_date', name: 'birth_date'},
                {data: 'gender', name: 'gender'},
                {data: 'action', name: 'action'}
            ]
        });
    </script>
@endpush