<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Place Of Birth</th>
        <th>Date Of Birth</th>
        <th>Gender</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->student_name }}</td>
        <td>{{ $model->birth_place }}</td>
        <td>{{ $model->birth_date }}</td>
        <td>{{ $model->gender }}</td>
    </tr>
</table>
