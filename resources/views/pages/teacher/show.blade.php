<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Place Of Birth</th>
        <th>Date Of Birth</th>
    </tr>
    <tr>
        <td>{{ $model->id }}</td>
        <td>{{ $model->teacher_name }}</td>
        <td>{{ $model->birth_place }}</td>
        <td>{{ $model->birth_date }}</td>
    </tr>
</table>
