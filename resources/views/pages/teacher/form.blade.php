{!! Form::model($model, [
    'route' => $model->exists ? ['teacher.update', $model->id] : 'teacher.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

    <div class="form-group">
        <label for="" class="control-label">Teacher Name</label>
        {!! Form::text('teacher_name', null, ['class' => 'form-control', 'id' => 'teacher_name']) !!}
    </div>
    <div class="form-group">
        <label for="" class="control-label">Place Of Birth</label>
        {!! Form::text('birth_place', null, ['class' => 'form-control', 'id' => 'birth_place']) !!}
    </div>
    <div class="form-group">
        <label for="" class="control-label">Date Of Birth</label>
        {!! Form::date('birth_date', null, ['class' => 'form-control', 'id' => 'birth_date']) !!}
    </div>

{!! Form::close() !!}
