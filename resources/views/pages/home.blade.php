@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Tables</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-4">
            <div class="well dash-box">
                <h2>
                    <a href="{{ route('teacher.index') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>   
                </h2>
                <h4>Teachers</h4>
            </div>
        </div>
        <div class="col-md-4">
            <div class="well dash-box">
                <h2>
                    <a href="{{ route('student.index') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>   
                </h2>
                <h4>Students</h4>
            </div>
        </div>
        <div class="col-md-4">
            <div class="well dash-box">
                <h2>
                    <a href="{{ route('class.index') }}"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>   
                </h2>
                <h4>Classes</h4>
            </div>
        </div>
    </div>
</div>
@endsection
