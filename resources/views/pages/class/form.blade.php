{!! Form::model($model, [
    'route' => $model->exists ? ['class.update', $model->id] : 'class.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

    <div class="form-group">
        <label for="" class="control-label">Class Name</label>
        <input type="text" name="class_name" value="{{ $model->class_name }}" class="form-control" id="class_name">
    </div>
    <div class="form-group">
        <label for="" class="control-label">Teacher Name</label>
        <select name="teacher_id" class="form-control" id="teacher_id">
            @foreach ($listTeacher as $teacher)
                <option @if ($teacher->id == $model->teacher_id) selected @endif value="{{ $teacher->id }}"> {{ $teacher->teacher_name }} </option>
            @endforeach
        </select>
    </div>

{!! Form::close() !!}
