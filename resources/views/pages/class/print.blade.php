<<!DOCTYPE html>
<html>
<head>
	<title> Class Management System</title>
</head>
<body>
    <div style='font-size: 9pt; padding-top: -45px'>CMS - Management Service</div>
		<table style='border-bottom: 1px black; padding-left: 170px; padding-top: -20px'>
			<tr>
				<td>
					<div style='font-weight: bold; padding-bottom: 2px; font-size: 12pt; text-align: center'>
						CLASS MANAGEMENT SYSTEM<br>
						LARAVEL TEST
					</div>
					<div style='font-weight: bold; font-size: 10pt; padding-bottom: 5px; text-align: center'>
						LIST CLASS DETAIL
					</div>
					<div style='font-size: 9pt; text-align: center'>
						Address : Indonesia - Manado 12345 Telphone : (00) 12345, Fax : (00) 12345
					</div>
				</td>
			</tr>
		</table>
		<table style='border-bottom: 4px black;  padding-right: 670px'>
		</table>

	    <p style='font-size: 12pt; text-align: center; font-weight: bold'>LIST CLASS DETAIL</p><br>

		<table cellpadding='4' cellspacing='0' style='border-top: 2px; border-bottom: 1px; border-right: 2px; border-left: 2px;'>
			<tr style='background-color: #fcf8e1; '>
				<th style='border-right: 1px; border-bottom: 1px; padding: 5px 0px 5px 0px; font-size: 11pt; text-align: center;'>No.</th>
				<th style='border-right: 1px; border-bottom: 1px; padding: 5px 0px 5px 0px; font-size: 11pt;'>Student Name</th>
				<th style='border-right: 1px; border-bottom: 1px; padding: 5px 0px 5px 0px; font-size: 11pt;'>Teacher Name</th>
				<th style='border-right: 1px; border-bottom: 1px; padding: 5px 0px 5px 0px; font-size: 11pt; text-align: center;'>Class Name</th>
			</tr>
			<?php $i=1; ?>
	        @foreach ($get_data as $get) 
			<tr align="top">
				<td style="border-right: 1px; border-bottom: 1px; width: 25mm; padding: 5px 0px 5px 5px; font-size: 10pt; text-align: center">{{$i}}</td>
				<td style="border-right: 1px; border-bottom: 1px; width: 60mm; padding: 5px 0px 5px 5px; font-size: 10pt;">{{ $get->student_name }}</td>
				<td style="border-right: 1px; border-bottom: 1px; width: 60mm; padding: 5px 0px 5px 5px; font-size: 10pt;">{{ $get->teacher_name }}</td>
				<td style="border-right: 1px; border-bottom: 1px; width: 45mm; padding: 5px 0px 5px 5px; font-size: 10pt; text-align: center">{{ $get->class_name }}</td>
			</tr>
			<?php $i++; ?>
			@endforeach
	    </table>
</body>
</html>       
		

