<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>Class Name</th>
        <th>Teacher Name</th>
    </tr>
    <tr>
        <td>{{ $class->id }}</td>
        <td>{{ $class->class_name }}</td>
        <td>{{ $class->teacher->teacher_name }}</td>
    </tr>
</table>
