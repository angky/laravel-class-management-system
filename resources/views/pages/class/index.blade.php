@extends('layouts.app')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Class Data
                <a href="{{ route('class.create') }}" class="btn btn-success pull-right modal-show" title="Create Class"><i class="icon-plus"></i> Add Class
                </a>
                <a href="{{ route('convertPdf') }}" class="btn btn-danger" title="Print Data">Print</a>
            </h3>
        </div>
        <div class="panel-body">
            <table id="datatable" class="table table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Class Name</th>
                        <th>Teacher Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                      
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Class Name</th>
                        <th>Teacher Name</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.class') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'class_name', name: 'class_name'},
                {data: 'teacher_name', name: 'teacher_name'},
                {data: 'action', name: 'action'}
            ]
        });
    </script>
@endpush