{!! Form::model($model, [
    'route' => $model->exists ? ['updateStudentClassId', $model->id] : '',
    'method' => $model->exists ? 'PUT' : ''
]) !!}

    <div class="form-group">
        <label for="" class="control-label">Student Name</label>
        <input type="hidden" name="class_id" value="{{ $model->id }}">
        <select name="student_id" class="form-control" id="student_id">
            @foreach ($listStudent as $student)
                <option value="{{ $student->id }}"> {{ $student->student_name }} </option>
            @endforeach
        </select>
        <br>
        <p>* Note : The data displayed are students who have not registered in any class.</p>
    </div>

{!! Form::close() !!}
