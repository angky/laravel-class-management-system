@extends('layouts.app')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
            @foreach ($detail as $detail)
                 Detail Class :
                <a href="{{ route('studentRegistration', [$detail->id]) }}" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Add Student"><i class="icon-plus"></i> Student Registration</a>
            @endforeach
            </h3>
        </div>
        <div class="panel-body">
            <h3>Class Name : {{ $detail->class_name }} <br> Teacher Name : {{$detail->teacher_name}}</h3>
            <table id="datatable" class="table table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Student Name</th>
                        <th>Gender</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                      
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Student Name</th>
                        <th>Gender</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('detailTable', [$detail->id]) }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'student_name', name: 'student_name'},
                {data: 'gender', name: 'gender'},
                {data: 'action', name: 'action'}
            ]
        });
    </script>
@endpush