<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return view('auth.login');
});

Auth::routes();

//Route as admin
Route::group(['middleware' => ['web','auth']], function()    
{
    Route::get('/home', 'AdminController@index')->name('home');

    Route::resource('/teacher', 'TeacherController');
    Route::get('/table/teacher', 'TeacherController@dataTable')->name('table.teacher');

    Route::resource('/student', 'StudentController');
    Route::get('/table/student', 'StudentController@dataTable')->name('table.student');

    Route::resource('/class', 'ClassController');
    Route::get('/table/class', 'ClassController@dataTable')->name('table.class');

    Route::get('/class/{id}/detail', 'ClassDetailController@detailClass')->name('detailClass');
    Route::get('/table/{id}/class/detail', 'ClassDetailController@dataTableClassDetail')->name('detailTable');
    Route::get('/class/{id}/register', 'ClassDetailController@studentRegistration')->name('studentRegistration');
    Route::put('/class/{id}/registered', 'ClassDetailController@studentClassUpdate')->name('updateStudentClassId');
    Route::delete('/class/{id}/delete', 'ClassDetailController@studentClassDelete')->name('destroyClassDetail');

    Route::get('/class/convert_pdf/pdf', 'AdminController@pdf')->name('convertPdf');
});
